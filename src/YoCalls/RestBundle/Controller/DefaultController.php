<?php

namespace YoCalls\RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use YoCalls\RestBundle\Entity\Contacts;

/**
 * Class DefaultController
 * @package YoCalls\RestBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/sync",methods={"GET"},name="rest_sync")
     * @return JsonResponse
     */
    public function syncAction()
    {
        $responseData = $this->get('service.guzzle_service')->getJson('http://beta.json-generator.com/api/json/get/Pt1p8fu');
        $response = $this->get('rest.service.synchronizer')->sync($responseData);
        return new JsonResponse($response);
    }

    /**
     * @Route("/",methods={"GET"},name="rest_index")
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $entities = $this->getDoctrine()->getRepository('YoCallsRestBundle:Contacts')->findAll();
        $response = $this->get('serializer')->serialize($entities, 'json');
        return new Response($response, Response::HTTP_OK, array('Content-Type' => 'text/javascript'));
    }

    /**
     * @Route("/",methods={"POST"},name="rest_add")
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $inputData = $request->getContent();
        $serializer = $this->get('serializer');
        $contact = $serializer->deserialize($inputData, Contacts::class, 'json');
        $validator = $this->get('validator');
        $errors = $this->get('service.error_formatter')->getErrorsArray($validator->validate($contact));

        if (count($errors) > 0) {
            return new JsonResponse($errors, 400);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();

        return new JsonResponse($contact->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/{id}",methods={"PUT"},name="rest_edit")
     * @param Contacts $contact
     * @param Request $request
     * @return JsonResponse
     */
    public function editAction(Contacts $contact, Request $request)
    {
        $inputData = $request->getContent();
        $serializer = $this->get('serializer');
        $serializer->deserialize($inputData, Contacts::class, 'json', array('object_to_populate' => $contact));
        $validator = $this->get('validator');
        $errors = $this->get('service.error_formatter')->getErrorsArray($validator->validate($contact));

        if (count($errors) > 0) {
            return new JsonResponse($errors, 400);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();

        return new JsonResponse($contact->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/{id}",methods={"GET"},name="rest_show")
     * @param Contacts $contact
     * @return JsonResponse
     * @internal param $id
     */
    public function showAction(Contacts $contact)
    {
        return new JsonResponse($contact->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/{id}",methods={"DELETE"},name="rest_delete")
     * @return JsonResponse
     */
    public function deleteAction(Contacts $contact)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($contact);
        $em->flush();
        return new JsonResponse(null, 204);
    }
}