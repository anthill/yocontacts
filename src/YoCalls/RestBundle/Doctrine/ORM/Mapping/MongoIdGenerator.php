<?php

namespace YoCalls\RestBundle\Doctrine\ORM\Mapping;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Saxulum\MongoId\MongoId;

/**
 * Mongo like id generator for Doctrine
 * Class MongoIdGenerator
 * @package YoCalls\RestBundle\Doctrine\ORM\Mapping
 */
class MongoIdGenerator extends AbstractIdGenerator
{

    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager|EntityManager $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return mixed
     */
    public function generate(EntityManager $em, $entity)
    {
        if ($entity->getId()) {
            return $entity->getId();
        }
        return (string)MongoId::createByDateTime(new \DateTime());
    }
}