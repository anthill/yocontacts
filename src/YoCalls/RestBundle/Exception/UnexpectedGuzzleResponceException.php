<?php

namespace YoCalls\RestBundle\Exception;

/**
 * Class UnexpectedGuzzleResponseException
 * @package YoCalls\RestBundle\Exception
 */
class UnexpectedGuzzleResponseException extends \Exception
{

}