<?php

namespace YoCalls\RestBundle\Service;


use YoCalls\RestBundle\Exception\UnexpectedGuzzleResponseException;

class GuzzleService
{

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzle;

    /**
     * GuzzleService constructor.
     * @param \GuzzleHttp\Client $guzzle
     */
    public function __construct(\GuzzleHttp\Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function getJson($url, array $params = array())
    {
        $res = $this->guzzle->request('GET', $url, $params);
        $body = $res->getBody()->getContents();
        $jsonEncoded = json_decode($body, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new UnexpectedGuzzleResponseException();
        }
        return $jsonEncoded;
    }
}