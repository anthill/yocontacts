<?php

namespace YoCalls\RestBundle\Service;


use Symfony\Component\Validator\ConstraintViolation;

class ErrorFormatterService
{

    public function getErrorsArray($errors)
    {
        if (empty($errors)) {
            return array();
        }
        $errorsArray = array();
        foreach ($errors as $error) {
            /* @var $error ConstraintViolation */
            $errorsArray[$error->getPropertyPath()] = $error->getMessage();
        }
        return $errorsArray;
    }
}