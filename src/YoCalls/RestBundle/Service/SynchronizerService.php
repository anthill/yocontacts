<?php

namespace YoCalls\RestBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Serializer\Serializer;
use YoCalls\RestBundle\Entity\Contacts;

/**
 * Class SynchronizerService
 * @package YoCalls\RestBundle\Service
 */
class SynchronizerService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SynchronizerService constructor.
     * @param EntityManager $entityManager
     * @param Serializer $serializer
     */
    public function __construct(EntityManager $entityManager, Serializer $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }


    /**
     * sync contact list between client and server
     * @param array $values
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function sync(array $values)
    {

        $newValues = array();

        $conflictValues = array();

        foreach ($values as $value) {
            /* @var $contact Contacts */
            $contact = $this->serializer->deserialize(json_encode($value), Contacts::class, 'json');
            $possibleSame = $this->entityManager->find('YoCallsRestBundle:Contacts', $contact->getId());

            if (!$possibleSame) {
                $this->entityManager->persist($contact);
                $this->entityManager->flush();
                $newValues[] = $contact->toArray();

                continue;
            }

            if (!$this->isSame($contact, $possibleSame)) {
                $conflictValues[] = array(
                    'local' => $possibleSame->toArray(),
                    'server' => $contact->toArray()
                );
                continue;
            }
        }
        return array('added' => $newValues, 'conflict' => $conflictValues);
    }

    /**
     * check object for identical
     * @param Contacts $contact
     * @param Contacts|null $possibleSameContact
     * @return bool
     */
    private function isSame($contact, $possibleSameContact)
    {
        $contactArray = $contact->toArray();
        $possibleArray = $possibleSameContact->toArray();
        foreach ($possibleArray as $key => $value) {
            if (!array_key_exists($key, $contactArray)) {
                continue;
            }
            if ($contactArray[$key] !== $value) {
                return false;
            }
        }
        return true;
    }
}