<?php
namespace YoCalls\RestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="contacts")
 * @HasLifecycleCallbacks
 */
class Contacts
{
    /**
     * @var string
     * @ORM\Column(type="string",name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="\YoCalls\RestBundle\Doctrine\ORM\Mapping\MongoIdGenerator")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="order_index",type="integer",nullable=true)
     * @Groups({"main"})
     */
    private $index;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="registered")
     */
    private $registered;

    /**
     * @var string
     * @ORM\Column(name="sip",type="string",nullable=true)
     * @Groups({"main"})
     */
    private $sip;

    /**
     * @var string
     * @ORM\Column(name="phone",type="string")
     * @Groups({"main"})
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="first_name",type="string")
     * @Groups({"main"})
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(name="last_name",type="string")
     * @Groups({"main"})
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(name="picture",nullable=true,type="string")
     * @Groups({"main"})
     */
    private $picture;

    /**
     * @var bool
     * @ORM\Column(name="is_active",type="boolean")
     * @Groups({"main"})
     */
    private $isActive = false;

    /**
     * @var string
     * @ORM\Column(name="guid",type="text",nullable=true)
     */
    private $guid; // "105f5cca-b587-4ccb-ae6e-e36ff325b2ce";

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered->getTimestamp();
    }

    /**
     * @param \DateTime $registered
     */
    public function setRegistered($registered)
    {
        if (!$registered instanceof \DateTime) {
            $registered = (new \DateTime())->setTimestamp($registered);
        }
        $this->registered = $registered;
    }

    /**
     * @return string
     */
    public function getSip()
    {
        return $this->sip;
    }

    /**
     * @param string $sip
     */
    public function setSip($sip)
    {
        $this->sip = $sip;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return boolean
     */
    public function isIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    public function setName(array $array = array())
    {
        if (array_key_exists('first', $array)) {
            $this->setFirstName($array['first']);
        }
        if (array_key_exists('last', $array)) {
            $this->setLastName($array['last']);
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function setRegisteredData()
    {
        if (!$this->registered) {
            $this->registered = new \DateTime();
        }
    }

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'index' => $this->getIndex(),
            'registered' => $this->getRegistered(),
            'sip' => $this->getSip(),
            'phone' => $this->getPhone(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'picture' => $this->getPicture(),
            'isActive' => $this->isIsActive(),
            'guid' => $this->getGuid()
        );
    }
}