app.filter('unixDateFormat', function () {
    return function (input, format) {
        if(!input){
            return '-';
        }
        return moment.unix(input).format(format);
    }
});