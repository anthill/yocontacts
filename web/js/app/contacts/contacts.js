app.controller('ContactsCtrl', function ($scope, $http) {
    $scope.name = 'Superhero';
    $scope.contactModalVisible = false;
    $scope.contactModal = '/js/app/views/contact.html';
    $scope.showContactModalVisible = false;
    $scope.showContactModal = '/js/app/views/show-contact.html';
    $scope.diffContactModalVisible = false;
    $scope.diffContactModal = '/js/app/views/diff-contact.html';
    $scope.isAdd = true;
    $scope.clients = [];
    $scope.conflicts = [];
    $scope.conflictCurrent = {};

    $scope.element = {};

    $scope.toggleModal = function (value) {
        $scope[value] = !$scope[value];
    };

    $scope.addContact = function () {
        $scope.isAdd = true;
        $scope.element = {};
        $scope.toggleModal('contactModalVisible');
    };

    $scope.edit = function (value) {
        $scope.isAdd = false;
        $scope.element = value;
        $scope.toggleModal('contactModalVisible');

    };

    $scope.send = function () {
        var method = 'POST';
        var url = '/';

        if (!$scope.isAdd) {
            method = 'PUT';
            url = '/' + $scope.element.id;
        }

        $http({
            method: method,
            url: url,
            data: $scope.element
        }).then(function successCallback(response) {
            if ($scope.isAdd) {
                $scope.clients.push(response.data);
            } else {
                var idx = $scope.clients.indexOf($scope.element);
                $scope.clients[idx] = response.data;
            }
            $scope.contactModalVisible = false;
        }, function errorCallback(response) {
            console.log(response);
        });
    };

    $scope.show = function (value) {
        $http({
            method: 'GET',
            url: '/' + value.id
        }).then(function successCallback(response) {
            $scope.element = response.data;
            $scope.toggleModal('showContactModalVisible');
        }, function errorCallback(response) {
        });
    };

    $scope.delete = function (value) {
        $http({
            method: 'DELETE',
            url: '/' + value.id
        }).then(function successCallback(response) {
            var idx = $scope.clients.indexOf(value);
            $scope.clients.splice(value, 1);
        }, function errorCallback(response) {
        });
    };


    $scope.conflictResolver = function () {
        if ($scope.conflicts.length <= 0) {
            $scope.diffContactModalVisible = false;
            return;
        }
        $scope.conflictCurrent = $scope.conflicts[$scope.conflicts.length - 1];
        if (!$scope.diffContactModalVisible) {
            $scope.diffContactModalVisible = true;
        }

    };

    $scope.conflictSkip = function () {
        $scope.conflicts.pop();
        $scope.conflictResolver();
    };

    $scope.conflictRewrite = function () {
        $scope.conflicts.pop();
        var idx = null;
        $scope.clients.forEach(function (el, key) {
            if (el.id === $scope.conflictCurrent.server.id) {
                idx = key;
            }
        });
        $scope.element = $scope.conflictCurrent.server;
        $scope.clients[idx] = $scope.element;
        $scope.isAdd = false;

        $scope.send();
        $scope.conflictResolver();
    };

    $scope.syncContacts = function () {
        if ($scope.conflicts.length > 0) {
            $scope.toggleModal('diffContactModalVisible');
            return;
        }
        $http({
            method: 'GET',
            url: '/sync'
        }).then(function successCallback(response) {
            if (response.data.added.length > 0) {
                response.data.added.forEach(function (element) {
                    $scope.clients.push(element);
                });
            }
            $scope.conflicts = response.data.conflict;
            $scope.conflictResolver();
        }, function errorCallback(response) {
        });
    };

    if ($scope.clients.length == 0) {
        $http({
            method: 'GET',
            url: '/'
        }).then(function successCallback(response) {
            $scope.clients = response.data;
        }, function errorCallback(response) {
        });
    }
});